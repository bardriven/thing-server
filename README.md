# thing-server

Client(스마트 공기청정기)의 실시간 데이터, Web(사용자) 제어 데이터, Tensorflow Serving(LSTM 모델) 예측 데이터를 서로 연결해주는 WebSocket 서버.


인증된 기기와 인증된 사용자가 서버에 접속 후 데이터 요청 및 교환.