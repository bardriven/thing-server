/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const subscribes = (thingName) => {
    return [
        "$aws/things/" + thingName + "/shadow/update/accepted",
        "$aws/things/" + thingName + "/shadow/update/rejected",
        "$aws/things/" + thingName + "/shadow/update/documents",
        "$aws/things/" + thingName + "/shadow/update/delta",
        "$aws/things/" + thingName + "/shadow/get/accepted",
        "$aws/things/" + thingName + "/shadow/get/rejected",
        "$aws/things/" + thingName + "/shadow/delete/accepted",
        "$aws/things/" + thingName + "/shadow/delete/rejected"
    ]
};

const publishes = (thingName) => {
    return [
        "$aws/things/" + thingName + "/shadow/update",
        "$aws/things/" + thingName + "/shadow/get"
    ]
};


/**
 *
 * @type {function(string): string[]}
 */
module.exports.subscribes = subscribes;
/**
 *
 * @type {function(string): string[]}
 */
module.exports.publishes = publishes;