/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const path = require("path");
const homedir = require("os").homedir();


const AWSConnectionInfo = { // 사물이 등록되는 AWS 계정의 인증 정보
    keyPath: path.join(homedir, "/.aws/thingCert/9eed442e8b-private.pem.key"),
    certPath: path.join(homedir, "/.aws/thingCert/9eed442e8b-certificate.pem.crt"),
    caPath: path.join(homedir, "/.aws/thingCert/VeriSign-Class 3-Public-Primary-Certification-Authority-G5.pem"),
    //clientId: "",
    host: "a2nu4sqxvvwc3q.iot.ap-northeast-2.amazonaws.com"
};

/**
 *
 * @type {{keyPath: string, certPath: string, caPath: string, host: string}}
 */
module.exports = AWSConnectionInfo;










/*
let s3 = new AWS.S3(clientConfig);
let listBuckets = s3.listBuckets().promise();
listBuckets.then(value => {
    console.log(value);
}, reason => {
    console.log(reason);
});

let params = {
    Bucket: "training-data-mital-app",
    Key: "Test1",
    Body: "TEs다"
};
let putObject = s3.putObject(params).promise();
putObject.then(value => {
    console.log(value);
}, reason => {
    console.log(reason);
});*/