/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

let AWS = require("aws-sdk");
let credentials = new AWS.SharedIniFileCredentials(); //todo 홈폴더에 .aws/credentials 파일 필요 (액세스 키 ID, 비밀 액세스 키)

/**
 *
 * @type {{aws: {region: string, credentials: SharedIniFileCredentials}, thingInfo: {thingTypeName: string, thingGroupName: string, thingGroupArn: string, principal: string}, bucket: {name: string}}}
 */
module.exports = { //todo 개인 aws 계정 정보에 맞게 수정 필요
    aws: {
        region: "ap-northeast-2",
        // 액세스 키 ID, 비밀 액세스 키
        credentials: credentials,
    },
    thingGroup: {
        // 사물 유형
        thingTypeName: "latteType",
        // 그룹 이름
        thingGroupName: "lattes",
        // 그룹 ARN
        thingGroupArn: "arn:aws:iot:ap-northeast-2:892575833585:thinggroup/lattes",
        // 인증서 ARN
        principal: "arn:aws:iot:ap-northeast-2:892575833585:cert/9eed442e8b216dd7468d482d3b32cdafdbeb1f98fac2c44df20616cc08770955"
    },
    bucket: {
        name: "training-data-mital-app"
    }
};






/*
let s3 = new AWS.S3(clientConfig);
let listBuckets = s3.listBuckets().promise();
listBuckets.then(value => {
    console.log(value);
}, reason => {
    console.log(reason);
});

let params = {
    Bucket: "training-data-mital-app",
    Key: "Test1",
    Body: "TEs다"
};
let putObject = s3.putObject(params).promise();
putObject.then(value => {
    console.log(value);
}, reason => {
    console.log(reason);
});*/