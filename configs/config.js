/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

module.exports.token = {
    secret: "SeCrEtKeYfOrHaShInG"
};

module.exports.SaveLogsConfig = {
    interval: 20000
};

/**
 *
 * @type {{APIKey: string, api_server: string, thing_server: string, strictSSL: boolean}}
 */
module.exports.api = {
    APIKey: "APIKeyAPIKey",
    api_server: "https://dev.osirptpm.ml:8091",
    thing_server: "https://dev.osirptpm.ml:8092",
    predict_server: "http://dev.osirptpm.ml:8093",
    strictSSL: true
};