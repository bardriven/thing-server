/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const jwt = require("jsonwebtoken");
const config = require("../configs/config");


module.exports = (socket, next) => {
    let tokens = socket.handshake.query.tokens;
    if (!tokens) next(new Error("인증 실패"));
    tokens = tokens.split(",");

    const verify = new Promise((resolve, reject) => {
        let decodeds = [];
        tokens.forEach(token => {
            try {
                let decoded = jwt.verify(token, config.token.secret);
                decodeds.push(decoded);
            } catch (e) { reject(e); }
        });
        resolve(decodeds);
    });

    const respond = (decodeds) => {
        socket.decodeds = decodeds;
        next();
    };

    verify
        .then(respond)
        .catch(next);
};