/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const SocketIO = require("socket.io");
const socketAuth = require("./middlewares/socketAuth");
const joinRoom = require("./helpers/socket/joinRoom");
const attachEvent = require("./helpers/socket/attachEvent");
const awsIoTController = require("./helpers/awsIoTController");

// AWS IoT or 사물로부터 직접
// 1. 상태 데이터를 받아, 권한이 있는 클라이언트(소켓 클라이언트들)에게 전달
// 2. 클라이언트가 전원을 조작하면 해당 명령을 AWS IoT or 사물로 전달
function socketIO(server) {
    let io = SocketIO(server);
    io.use(socketAuth);
    let awsIoTDevice = awsIoTController();
    io.on("connection", (socket) => {
        // console.log(socket.id, " user connected");
        joinRoom(socket);
        attachEvent(socket, awsIoTDevice);
    });
}

module.exports = socketIO;