/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const fs = require("fs");
const path = require("path");

class SaveLogs {
    /**
     * @callback anotherDayCallback
     * @param {string} pathname
     * @param {string} filename
     */
    /**
     *
     * @param {anotherDayCallback} callback
     */
    set anotherDay(callback) {
        this._anotherDay = callback;
    }

    constructor(dir) {
        this._init(dir);
    }

    _init(p) {
        this.sDay = new Date().getTime();

        // logs 폴더 생성
        this.dir = path.normalize(p);
        try {
            fs.mkdirSync(this.dir);
        } catch (e) {
            if (e.code === "EEXIST") { // logs 폴더 이미 있음
                let filenames = fs.readdirSync(this.dir);
                if (filenames.length !== 0) {
                    let filename = path.parse(filenames[filenames.length - 1]).name;
                    let milliseconds = parseInt(filename.substring(filename.indexOf("_") + 1, filename.length));
                    // 같은 날이면 마지막 로그파일에 이어서 저장
                    if (this._checkSameDay(milliseconds, this.sDay)) this.sDay = milliseconds;
                }
            } else console.log(e);
        }
        this.filename = "log_" + this.sDay;
    }

    _checkSameDay(d1, d2) {
        d1 = new Date(d1); d2 = new Date(d2);
        return d1.getFullYear() === d2.getFullYear() &&
            d1.getMonth() === d2.getMonth() &&
            d1.getDate() === d2.getDate();
    }

    //saveToJSON(data) {
        //fs.appendFileSync("/logs/data.json", JSON.stringify(data) + ",\r\n", 'utf8');
    //}
    appendToCSV(str) {
        if (!this._checkSameDay(this.sDay, new Date().getTime())) {
            this._anotherDay(path.join(this.dir, this.filename +".csv"), this.filename +".csv");
            this.sDay = new Date().getTime();
            this.filename = "log_" + this.sDay;
        }
        //let str = "";
        //str += data.temperature + "," + data.humidity + "," + data.pm2_5 + "," + data.pm10 + "," + data.power + "," + data.time + "\r\n";
        fs.appendFileSync(path.join(this.dir, this.filename +".csv"), str.trim() + "\r\n", "utf8");
    }

    /**
     *
     * @param {{temperature: number, humidity: number, pm2_5: number, pm10: number, power: boolean, time: number}} data
     * @return {string}
     * @constructor
     */
    JSONToCSV(data) {
        return data.temperature + "," + data.humidity + "," + data.pm2_5 + "," + data.pm10 + "," + data.power + "," + data.time;
    }
}

module.exports = SaveLogs;