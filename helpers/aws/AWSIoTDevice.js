/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const awsIot = require("aws-iot-device-sdk");


class AWSIoTDevice {
    /**
     *
     * @return {boolean}
     */
    get isConnected() {
        return this._isConnected;
    }

    /**
     * @callback messageCallback
     * @param {string} topic
     * @param {Buffer} payload
     */
    /**
     *
     * @param {messageCallback} value (topic, payload)
     */
    set onMessage(value) {
        this._onMessage = value;
        let device = this._device;
        device.on("message", this._onMessage);
    }

    constructor() {
    }


    /**
     *
     * @param {{keyPath: string, certPath: string, caPath: string, host: string}} connectionInfo
     * @returns {Promise<>}
     */
    connect(connectionInfo) {
        this._device = awsIot.device(connectionInfo);
        let device = this._device;
        return new Promise(resolve => {
            device.on("connect", () => {
                this._isConnected = true;
                resolve();
            });
        });

        /*device.on("message", function(topic, payload) {
            console.log("message", topic);
            console.table(JSON.parse(payload.toString()));
        });*/
    }

    /**
     *
     * @param {string || Array<string>} topic
     */
    subscribe(topic) {
        if (!this._isConnected) throw new Error("Not connected");

        let subTopics = [];
        let device = this._device;

        if (topic.constructor.name === "Array") subTopics.push(...topic);
        else if (topic.constructor.name === "String") subTopics.push(topic);

        subTopics.forEach(subTopic => {
            device.subscribe(subTopic);
        });
    }

    /**
     *
     * @param {string || Array<string>} topic
     * @param {{ state: { reported: object, desired: object }, clientToken: string }} value
     */
    publish(topic, value) {
        if (!this._isConnected) return false;

        let pubTopics = [];
        let device = this._device;

        if (topic.constructor.name === "Array") pubTopics.push(...topic);
        else if (topic.constructor.name === "String") pubTopics.push(topic);

        pubTopics.forEach(pubTopic => {
            device.publish(pubTopic, JSON.stringify(value));
        });
    }
}
module.exports = AWSIoTDevice;