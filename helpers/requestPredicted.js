/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const request = require("request");
const config = require("../configs/config");


function normalizingTime(time) {
    return Math.round((time + 32400000) / 1000 % 86400 / 20) / 4320 * 100;
}

function squeeze(arr) {
    let result = [];
    let zero = 1e-10;
    arr.forEach(shadow => {
        let reported = shadow.state.reported;
        result.push([reported.temperature + zero, reported.humidity + zero, reported.pm2_5 + zero, reported.pm10 + zero, (reported.power ? 1 : 0) + zero, normalizingTime(reported.time) + zero]);
    });
    // result.push([zero,zero,zero,zero,zero,zero]);
    return result;
}

function processingData(result) {
    let zero = 1e-10;
    return {
        "instances": [{
            "lstm1_X": squeeze(result),
            "Keep_prob": 1.0 - zero
        }]
    };
}

/**
 *
 * @return {Promise<Array>} predict
 * @param name
 * @param result
 */
function requestPredictedPm(name, result) {
    let path = "/v1/models/" + name + ":predict"; // http://localhost:8501/v1/models/latte_1_pm25:predict
    let requestOptions = {
        url: config.api.predict_server + path,
        method: "POST",
        json: processingData(result),
        strictSSL: false //config.api.strictSSL
    };
    return new Promise((resolve, reject) => {
        request(requestOptions, (err, response, result) => {
            if (err) reject(err);
            if (response) {
                if (response.statusCode === 200) {
                    resolve(result);
                } else {
                    // console.log(result.message, response.statusCode);
                    reject(new Error(response.statusCode));
                }
            } else {
                // console.log(result.message, response.statusCode);
                reject(new Error("unknown error: Cannot read object 'response' of undefined"));
            }
        })
    });
}

/**
 *
 * @return {Promise<Array>} predict
 * @param name
 * @param result
 */
function requestPredicted(name, result) {
    // let path = "/v1/models/" + name + ":predict"; // http://localhost:8501/v1/models/latte_1:predict
    // let requestOptions = {
    //     url: config.api.predict_server + path,
    //     method: "POST",
    //     json: processingData(result),
    //     strictSSL: false //config.api.strictSSL
    // };

    return new Promise((resolve, reject) => {
        const chain = prediction => {
            requestPredictedPm(name + "_pm10", result).then(prediction1 => {
                for (let i = 0; i < prediction.predictions[0].length; i++) {
                    prediction.predictions[0][i].push(prediction1.predictions[0][i][0])
                }
                resolve(prediction);
            });
        };
        requestPredictedPm(name + "_pm25", result).then(chain);
    });
    //
    // return new Promise((resolve, reject) => {
    //     request(requestOptions, (err, response, result) => {
    //         if (err) reject(err);
    //         if (response) {
    //             if (response.statusCode === 200) {
    //                 resolve(result);
    //             } else {
    //                 // console.log(result.message, response.statusCode);
    //                 reject(new Error(response.statusCode));
    //             }
    //         } else {
    //             // console.log(result.message, response.statusCode);
    //             reject(new Error("unknown error: Cannot read object 'response' of undefined"));
    //         }
    //     });
    // });
}

module.exports = requestPredicted;