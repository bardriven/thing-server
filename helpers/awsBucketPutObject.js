/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const fs = require("fs");
const AWS = require("aws-sdk");
const AWSConfig = require("../configs/aws/AWSConfig");

let s3 = new AWS.S3(AWSConfig.aws);

module.exports = (path, key) => {
    let param = {
        Bucket: AWSConfig.bucket.name,
        Key: key,
        Body: fs.createReadStream(path)
    };
    s3.putObject(param, function(err, data){
        if(err) console.warn(err);
        else console.log(data);
    });
};