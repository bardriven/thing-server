/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const path = require("path");
const fs = require("fs");


class GetLogs {
    constructor(dir) {
        this._realTimeDTH = undefined;
        this._dir = dir;
    }

    get realTimeDTH() {
        return this._realTimeDTH;
    }

    /**
     * @callback dataCallback
     * @param {Error} err
     * @param {{data: [string], residualLine: number} || null} data
     */
    /**
     *
     * @param {dataCallback} callback
     * @param {number} [line]
     */
    getNormalizedDTH(callback, line = 50) {
        let logsPath = path.join(this._dir);
        let filePaths = this._getFilenames(logsPath);
        if (filePaths.length !== 0) {
            this._getDataFromTheEnd(filePaths, line, callback, { data: [], residualLine: 0 });
        } else {
            callback(new Error("No log file."), null);
        }
    }

    /**
     *
     * @param {[string]} filePaths
     * @param {number} line
     * @param {dataCallback} callback
     * @param {{data: [string], residualLine: number}} result
     * @private
     */
    _getDataFromTheEnd(filePaths, line, callback, result) {
        if (filePaths.length === 0) {
            callback(null, result);
            return;
        }
        fs.readFile(filePaths[filePaths.length - 1], "utf8", (err, data) => {
            let resultTmp = this._getLastLine(data, line);
            resultTmp.data = resultTmp.data.map(value => {
                return this._DTHToJSON(value);
            });
            result.data = resultTmp.data.concat(result.data);
            result.residualLine = resultTmp.residualLine;
            if (result.residualLine === 0) {
                callback(err, result);
                return;
            }
            filePaths.pop();
            this._getDataFromTheEnd(filePaths, result.residualLine, callback, result);
        });
    }


    /**
     *
     * @param {string} data
     * @param {number} line
     * @returns {{data: [string], residualLine: number}}
     * @private
     */
    _getLastLine(data, line) {
        let arr = data.split("\r\n");
        let startIndex = (arr.length - line - 1 < 0) ? 0 : arr.length - line - 1;
        let endIndex = arr.length - 1;
        let residualLine = (line  > arr.length - 1) ? line - (arr.length - 1) : 0;
        return { data: arr.slice(startIndex, endIndex), residualLine: residualLine };
    }

    /**
     *
     * @param arr
     * @return {{state: {reported: {temperature: number, humidity: number, pm2_5: number, pm10: number, power: boolean, time: number}, desired: {}}, clientToken: string}}
     * @private
     */
    _DTHToJSON(arr) {
        let data = arr.split(",");
        return {
            state: {
                reported: {
                    temperature: parseFloat(data[0]),
                    humidity: parseFloat(data[1]),
                    pm2_5: parseFloat(data[2]),
                    pm10: parseFloat(data[3]),
                    power: data[4] === "true",
                    time: parseFloat(data[5])
                },
                desired: {}
            },
            clientToken: ""
        };
    }

    /**
     *
     * @private
     * @param {string} logsPath
     * @return {[string]} filePaths
     */
    _getFilenames(logsPath) {
        let filenames = fs.readdirSync(logsPath, "utf8");
        let filePaths = [];
        for (let i = 0; i < filenames.length; i++) {
            filePaths.push(path.join(logsPath, filenames[i]));
        }
        return filePaths;
    }
}

module.exports = GetLogs;