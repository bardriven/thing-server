/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const normalizeData = require("../normalizeData");
const sendMessageByData = require("../sendMessageByData");
const DTHShadow = require("../../configs/aws/DTHShadow");
const ThingTopics = require("../../configs/aws/ThingTopics");
const {SaveLogsConfig} = require("../../configs/config");

/**
 *
 * @param socket
 * @param awsIoTDevice
 */
module.exports = (socket, awsIoTDevice) => {
    let from = socket.handshake.query.from;

    if (from === "client") clientAttachEvent(socket, awsIoTDevice);
    if (from === "thing") thingAttachEvent(socket, awsIoTDevice);

    socket.on("disconnect", (reason) => {
        console.log("disconnect: ", reason);
    });
    socket.on("error", (error) => {
        console.log("error: ", error);
    });
    socket.on("disconnecting", (reason) => {
        console.log("disconnecting: ", reason);
    });
};

function thingAttachEvent(socket, awsIoTDevice) {

    /**
     * @type {{name: string, line: number}} data
     */
    socket.on("predictDTH", (data) => {
        let name = socket.decodeds[0].name;
        if (name === data.name) {
            socket.predictInputLine = data.line;
            console.log(name + "의 predictDTH 길이: " + socket.predictInputLine);
        } else console.warn("predictDTH 길이 설정: 올바르지 않은 사물 이름");
    });

    /**
     * @type {{state: {reported: {temperature: number, humidity: number, pm2_5: number, pm10: number, power: boolean}, desired: {power: boolean}}, clientToken: string}} data
     */
    socket.on("realTimeDTH", (data) => {
        let name = socket.decodeds[0].name;


        // interval(20초) 간격으로 저장
        let normalizedData = normalizeData(socket, data.state.reported, SaveLogsConfig.interval);
        if (normalizedData) {
            socket.saveLogs.appendToCSV(socket.saveLogs.JSONToCSV(normalizedData));
            console.log(name + "평균 값 기록");
        }

        sendMessageByData(socket, data, normalizedData, awsIoTDevice);


        let room = name;
        let realTimeJson = {
            name: socket.decodeds[0].name,
            data
        };

        // 연결된 자기 room 에 데이터 브로드캐스팅
        socket.to(room).emit("realTimeDTH", realTimeJson);
        console.log(room + "의 방에 realTimeDTH 방송");
    });


    /**
     * @type {{error: string}} data
     */
    socket.on("power", (data) => {
        let name = socket.decodeds[0].name;
        let json = {
            name,
            error: data.error
        };
        socket.to(name).emit("power", json);
    });
}

/**
 *
 * @param socket
 * @param {AWSIoTDevice} awsIoTDevice
 */
function clientAttachEvent(socket, awsIoTDevice) {

    /**
     * @type {{name: string, power: boolean}} data
     */
    socket.on("power", (data) => {
        let name = data.name;

        let thing = socket.decodeds.find(decoded => decoded.name === name);
        let shadow = DTHShadow();
        delete shadow.state.reported;
        shadow.clientToken = thing._id;
        shadow.state.desired["power"] = data.power;
        awsIoTDevice.publish(ThingTopics.publishes(name)[0], shadow); // AWS IoT로 전원 요청

        let json = {
            power: data.power
        };
        socket.to(name).emit("power", json); // 소켓으로 전원 요청
        console.log("클라이언트의 " + name + "'s power 요청: " + data.power);
    });

    // /**
    //  * @type {{name: string, data: Array}} data
    //  */
    // socket.on("predictDTH", (data) => {
        // console.log(data);
        // let name = data.name;
        //
        // let thing = socket.decodeds.find(decoded => decoded.name === name);
        //
        // let json = {
        //     name: name,
        //     error: null,
        //     data: null,
        // };
        // if (thing) {
        //     requestPredicted(name, data.data).then(prediction => {
        //         json.data = prediction;
        //         socket.emit("predictDTH", json);
        //         console.log("클라이언트가 요청한 " + name + "의 predictDTH 응답");
        //     });
        // } else {
        //     json.error = new Error("올바른 이름이 아닙니다.");
        //     socket.emit("predictDTH", json);
        // }
    //
    // });

    // /**
    //  * @type {{name: string, line: number}} data
    //  */
    // socket.on("normalizedDTH", (data) => {
        // let line = data.line;
        // let name = data.name;
        //
        // let normalizedDTHJson = {
        //     name: name,
        //     error: null,
        //     data: null,
        //     interval: SaveLogsConfig.interval
        // };
        // if (!socket.getLogsObj[name]) {
        //     normalizedDTHJson.error = new Error("올바른 이름이 아닙니다.");
        //     socket.emit("normalizedDTH", normalizedDTHJson);
        //     return;
        // }
        // socket.getLogsObj[name].getNormalizedDTH((error, result) => {
        //     if (error) {
        //         normalizedDTHJson.error = error.message;
        //         socket.emit("normalizedDTH", normalizedDTHJson);
        //         return;
        //     }
        //     normalizedDTHJson.data = result.data;
        //     socket.emit("normalizedDTH", normalizedDTHJson);
        //     console.log("클라이언트가 요청한 " + name + "의 normalizedDTH 응답");
        // }, line);
        //
        // let predictDTHJson = {
        //     name: name,
        //     error: null,
        //     data: null,
        //     line: line || 50,
        // };
        // socket.to(name).emit("predictDTH", predictDTHJson);
        // console.log(name + "의 predictDTH 길이 요청 " + line);
    //
    // });
}