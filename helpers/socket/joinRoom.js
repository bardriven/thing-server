/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const SaveLogs = require("../SaveLogs");
const GetLogs = require("../GetLogs");
const awsBucketPutObject = require("../awsBucketPutObject");

module.exports = (socket) => {
    let from = socket.handshake.query.from;
    if (from === "thing") thingJoin(socket);
    if (from === "client") clientJoin(socket);
    // console.log(socket.decodeds, socket.handshake.query);
};


function thingJoin(socket) {
    let name = socket.decodeds[0].name;

    socket.DTHs = []; // 평균 값 구할때 필요

    socket.isFirst = true;
    socket.isFirstRunning = false;
    socket.predictInputs = undefined;
    socket.predictRealTimeInputs = undefined;
    socket.predictOutputs = undefined; // Array
    socket.normalizedData = undefined;
    socket.isFirstRealTime = true; // 실시간 예측 해야하는지
    socket.predictInputLine = 50; // 예측 요청 입력 길이

    // socket.predictInput = []; // 예측 요청 입력값
    // socket.predictInputLine = 50; // 예측 요청 입력 길이
    // socket.isFirstBeginningRun = true; // 예측 요청 입력 길이
    // socket.isFirstBeginningRunning = false; // 예측 요청 입력 길이
    // socket.predictOutput = []; // 예측 요청 출력
    // socket.isFirstRealTime = true; // 실시간 예측 해야하는지


    socket.saveLogs = new SaveLogs(__dirname + "../../../logs/" + name);
    socket.saveLogs.anotherDay = (pathname, filename) => {
        awsBucketPutObject(pathname, "train/" + name + "/" + filename);
        console.log(name + "의 일일 데이터 AWS Bucket 에 업로드: " + filename);
    };
    socket.getLogs = new GetLogs(__dirname + "../../../logs/" + name);
    socket.join(name, () => {
        console.log("사물이 ", name, "의 방에 입장");
    });
}

function clientJoin(socket) {
    let things = socket.decodeds;

    /**
     *
     * @type  {Object<GetLogs>}
     */
    socket.getLogsObj = {};

    things.forEach(thing => {
        let room = thing.name;

        // 사물 이름을 경로로 log 저장 객체 생성
        socket.getLogsObj[room] = new GetLogs(__dirname + "../../../logs/" + room);

        // 각 사물의 방에 가입
        socket.join(room, () => console.log("클라이언트가 " + room + "의 방에 입장"));
    });
}