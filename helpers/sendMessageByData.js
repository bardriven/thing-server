/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const requestPredicted = require("./requestPredicted");
const {SaveLogsConfig} = require("../configs/config");
const DTHShadow = require("../configs/aws/DTHShadow");
const ThingTopics = require("../configs/aws/ThingTopics");

function dataObj() {
    return {
        _callback: () => {},
        set callback(value) {
            this._callback = value;
        },
        call() {
            this._callback(this._values);
        },
        set inputs(values) {
            this._values = values;
            this._callback(this._values);
        },
        get inputs() {
            return this._values;
        },
        add(value, runCallback=true) {
            this._values.shift();
            this._values.push(value);
            if (runCallback) this._callback(this._values);
        },
        editLastData(data, runCallback=true) {
            this._values[this._values.length - 1] = data;
            if (runCallback) this._callback(this._values);
        }
    };
}


module.exports = function (socket, data, normalizedData, awsIoTDevice) {
    let name = socket.decodeds[0].name;

    if (normalizedData) {
        socket.isFirst = true;
        socket.isFirstRunning = false;
        socket.isFirstRealTime = true;
        socket.predictInputs = undefined;
        socket.predictRealTimeInputs = undefined;
        socket.predictOutputs = undefined;
        socket.normalizedData = undefined;
    }

    if (socket.isFirst) {
        if (socket.isFirstRunning) return;
        socket.isFirstRunning = true;
        socket.getLogs.getNormalizedDTH((error, result) => {
            if (error) {
                console.error(error);
                return;
            }
            let normalizedData = dataObj();
            normalizedData.callback = sendNormalizedDTH;
            normalizedData.inputs = result.data;
            socket.normalizedData = normalizedData;

            let predictData = JSON.parse(JSON.stringify(result.data)); // deep copy
            let predictInput = dataObj();
            predictInput.callback = sendPredictDTH;
            predictInput.inputs = predictData;
            socket.predictInputs = predictInput;

            let predictRealTimeData = JSON.parse(JSON.stringify(result.data.slice(result.data.length - socket.predictInputLine))); // deep copy
            let predictRealTimeInput = dataObj();
            predictRealTimeInput.inputs = predictRealTimeData;
            socket.predictRealTimeInputs = predictRealTimeInput;

            socket.isFirst = false;
        }, socket.predictInputLine * 2 - 1); // 그래프 내의 모든 예측 값을 위해
    } else {
        if (!socket.predictInputs || !socket.normalizedData || !socket.predictOutputs || !socket.predictRealTimeInputs) return;
        if (socket.isFirstRealTime) {
            socket.normalizedData.add(data);
            sendPredictDTH(socket.predictInputs.inputs, false);
            socket.predictRealTimeInputs.add(data, false);
            mergePredictDTH(socket.predictRealTimeInputs.inputs, socket.isFirstRealTime);
            // socket.predictInputs.add = data;
            // socket.normalizedData.call();
            socket.isFirstRealTime = false;
        } else {
            socket.normalizedData.editLastData(data);
            socket.predictRealTimeInputs.editLastData(data, false);
            mergePredictDTH(socket.predictRealTimeInputs.inputs, socket.isFirstRealTime);
        }
    }

    function mergePredictDTH(inputs, isFirstRealTime) {
        if (!socket.predictOutputs) return;
        let predictJson = {
            name: name,
            error: null,
            data: null
        };
        requestPredicted(name, inputs).then(prediction => {
            let lastData = prediction.predictions[0];
            lastData = lastData[lastData.length - 1];
            powerCtrl(lastData);
            if (isFirstRealTime) socket.predictOutputs.add(lastData);
            else socket.predictOutputs.editLastData(lastData);
            predictJson.data = {
                predictions: [socket.predictOutputs.inputs]
            };
            socket.to(name).emit("predictDTH", predictJson);
            console.log(name + "의 방에 predictDTH 방송");
        });
    }
    function sendPredictDTH(inputs, isCallback=true) {
        let predictJson = {
            name: name,
            error: null,
            data: null
        };
        requestPredicted(name, inputs).then(prediction => {
            if (isCallback) {
                socket.predictOutputs = dataObj();
                socket.predictOutputs.inputs = prediction.predictions[0];
            }
            predictJson.data = prediction;
            let lastData = prediction.predictions[0];
            lastData = lastData[lastData.length - 1];
            powerCtrl(lastData);
            socket.to(name).emit("predictDTH", predictJson);
            console.log(name + "의 방에 predictDTH 방송");
        });
    }
    function sendNormalizedDTH(data) {
        let normalizedDTHJson = {
            name: name,
            error: null,
            data: null,
            interval: SaveLogsConfig.interval
        };
        normalizedDTHJson.data = data;
        socket.to(name).emit("normalizedDTH", normalizedDTHJson);
        console.log(name + "의 방에 normalizedDTH 방송");
    }

    function powerCtrl(data) {
        let pm2_5 = data[0];
        let pm10 =  data[1];
        if (pm2_5 >= 30 || pm10 >= 81) {
        // if (pm2_5 >= 76 || pm10 >= 151) {
            let json = {
                power: true
            };
            socket.emit("power", json); // 소켓으로 전원 요청
            console.log("클라이언트의 " + name + "'s power 요청: " + json.power);

            let token = socket.decodeds[0]._id;
            let shadow = DTHShadow();
            delete shadow.state.reported;
            shadow.clientToken = token;
            shadow.state.desired["power"] = json.power;
            // awsIoTDevice.publish(ThingTopics.publishes(name), shadow)
        } else if (pm2_5 < 25 || pm10 < 35) {
            let json = {
                power: false
            };
            socket.emit("power", json); // 소켓으로 전원 요청
            console.log("클라이언트의 " + name + "'s power 요청: " + json.power);

            let token = socket.decodeds[0]._id;
            let shadow = DTHShadow();
            delete shadow.state.reported;
            shadow.clientToken = token;
            shadow.state.desired["power"] = json.power;
            // awsIoTDevice.publish(ThingTopics.publishes(name), shadow)
        }
    }
};