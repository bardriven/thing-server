
/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

/**
 *
 * @param {object} socket
 * @param {{temperature: number, humidity: number, pm2_5: number, pm10: number, power: boolean}} data
 * @param {number} interval
 * @return {undefined || {temperature: number, humidity: number, pm2_5: number, pm10: number, power: boolean, time: number}}
 */

module.exports = function normalizeData(socket, data, interval) {
    let avg = undefined;
    data["time"] = new Date().getTime();
    if (socket.DTHs.length !== 0) {
        let first = socket.DTHs[0].time - 1000;
        let last = data.time;
        if (last - first >= interval) { // 지정된 시간이 지남
            let sum = socket.DTHs.reduce((accumulator, currentValue) => {
                Object.keys(accumulator).map(k => {
                    if (k !== "power" || k !== "time") accumulator[k] += currentValue[k];
                });
                return accumulator;
            });

            avg = {
                power: socket.DTHs[socket.DTHs.length - 1].power,
                time: socket.DTHs[socket.DTHs.length - 1].time
            };

            delete sum.power; // sum === socket.DTHs[0]
            delete sum.time;
            Object.keys(sum).map(k => {
                avg[k] = parseFloat((sum[k] / socket.DTHs.length).toFixed(2));
            });
            socket.DTHs = [];
        }
    }
    socket.DTHs.push(data);
    return avg;
};
