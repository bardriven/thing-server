/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const request = require("request");
const ioClient = require("socket.io-client");
const AWSIoTDevice = require("./aws/AWSIoTDevice");
const AWSConnectionInfo = require("../configs/aws/AWSConnectionInfo");
const ThingTopics = require("../configs/aws/ThingTopics");
const config = require("../configs/config");


let things = {};

/**
 *
 * @param _id
 * @param name
 * @return {Promise<string>} token
 */
function lookupThingToken(_id, name) {
    let path = "/things/auth/token";
    let requestOptions = {
        url: config.api.api_server + path + "?apikey=" + config.api.APIKey,
        method: "POST",
        json: { thing: { _id, name } },
        strictSSL: config.api.strictSSL
    };

    return new Promise((resolve, reject) => {
        request(requestOptions, (err, response, result) => {
            if (err) reject(err);
            if (response.statusCode === 200 && result.success) {
                resolve(result.data);
            } else {
                // console.log(result.message, response.statusCode);
                reject(new Error(response.statusCode));
            }
        })
    });
}

/**
 *
 * @param token
 * @return {socket}
 */
function connectSocket(token) {
    let socket = ioClient(config.api.thing_server, {
        query: {
            // tokens: ["eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YjcwMDhjYzllMTBkNDA5OGNlZDNkYzQiLCJuYW1lIjoibGF0dGVfMiIsImtleSI6IkVTRC1BU0RGIiwiaWF0IjoxNTM0MDY4OTQwLCJpc3MiOiJtaXRhbCIsInN1YiI6InRoaW5nSW5mbyJ9.EwOrjQiUNU-mh9U9QU3sjnV7lc9f6vRUdxkS16-kRks"],
            tokens: [token],
            from: "thing"
        }
    });
    socket.on("connect", () => {
        console.log("thing-server connected");
    });
    socket.on("power", (data) => {
        console.log(data);
        // socket.disconnect();
    });
    socket.on("error", (reason) => {
        console.log("err: ", reason.message);
        socket.disconnect();
    });
    socket.on("disconnect", (reason) => {
        console.log("disconnect, reason: ", reason);
    });
    return socket;
}

/**
 *
 * @param topic
 * @param payload
 */
function handler(topic, payload) {
    let thingName = topic.split("/")[2];
    if (ThingTopics.subscribes(thingName)[0] === topic) { // topic === .../update/accepted
        let thingName = topic.split("/")[2];
        let shadow = JSON.parse(payload.toString());
        let thingId = shadow.clientToken;

        if (shadow.state.desired.power !== undefined) return;
        if (!things[thingName]) { // 처음 받은 이벤트라면..
            things[thingName] = {};
            things[thingName].createdTime = new Date().getTime();
            lookupThingToken(thingId, thingName).then(token => {
                things[thingName].socket = connectSocket(token);
                things[thingName].socket.on("connect", () => {
                    things[thingName].socket.emit("realTimeDTH", shadow);
                });
                things[thingName].socket.on("disconnect", (reason) => {
                    delete things[thingName];
                });
            });
        } else {
            if (things[thingName].socket && things[thingName].socket.connected) {
                things[thingName].createdTime = new Date().getTime();
                things[thingName].socket.emit("realTimeDTH", shadow);
            }
        }
    }
}

// 매 5초 마다 검사
// 60초간 새로운 메세지를 받지 못하면 소켓 연결 종료 => delete thing[name]
setInterval(function cleanupSocket() {
    for (let prob in things) {
        let now = new Date().getTime();
        if (things[prob].createdTime < now - 60000) {
            console.log("사용되지 않는 연결 제거:", prob);
            things[prob].socket.close();
        }
    }
}, 5000);

// 모든 사물을 전부 subscribe 해서
// 이벤트가 발생하는 사물만 클라이언트 소켓을 만들어 thing-server 에 연결
// 실제 사물에서 socket 연결한 것과 동일하게 처리됌
/**
 *
 * @return {AWSIoTDevice}
 */
module.exports = () => {
    let awsIoTDevice = new AWSIoTDevice();
    awsIoTDevice.connect(AWSConnectionInfo)
        .then(() => {
            console.log("AWS IoT Device 연결");
            awsIoTDevice.subscribe(ThingTopics.subscribes("+"));
            awsIoTDevice.onMessage = handler;
        })
        .catch((e) => {
            console.warn("AWS IoT Device 실패:", e);
        });
    return awsIoTDevice;
};

